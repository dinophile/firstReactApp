// var React = require('react');
// var NotesList = require('./NotesList');
// var AddNote = require('./AddNote');
import React from 'react';
import NotesList from './NotesList';
import AddNote from './AddNote';

class Notes extends React.Component {
  render(){
    console.log('Notes :', this.props.notes);
    return (
      <div>
        <h3>Notes for {this.props.username}</h3>
        <AddNote username={this.props.username} addNote={this.props.addNote} />
        <NotesList notes={this.props.notes}/>
      </div>
    )
  }
}

Notes.propTypes = {
  username: React.PropTypes.string.isRequired,
  notes: React.PropTypes.array.isRequired,
  addNote: React.PropTypes.func.isRequired
}

export default Notes;

// var Notes = React.createClass({
//   propTypes: {
//     username: React.PropTypes.string.isRequired,
//     notes: React.PropTypes.array.isRequired,
//     addNote: React.PropTypes.func.isRequired
//   },
//   render: function(){
//     console.log('Notes :', this.props.notes);
//     return (
//       <div>
//         <h3>Notes for {this.props.username}</h3>
//         <AddNote username={this.props.username} addNote={this.props.addNote} />
//         <NotesList notes={this.props.notes}/>
//       </div>
//     )
//   }
// });
//
// module.exports = Notes;
