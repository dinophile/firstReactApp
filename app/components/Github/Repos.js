// var React = require('react');
import React from 'react';

// Not able to refactor this to use statless components yet! 
class Repos extends React.Component {
  render(){
    var repos  = this.props.repos.map(function(repo, index){
      var url;
      if (repo.html_url){
        url = <h4><a href={repo.html_url}>{repo.name}</a></h4>;
      }
      return (
        <li className="list-group-item" key={index}>
          {url}
          {repo.description && <p>{repo.description}</p>}
        </li>
     );
   });
    return (
      <div>
        <h3>User Repos</h3>
        <ul className="list-group">
          {repos}
        </ul>
      </div>
    )
  }
}

Repos.propTypes = {
  username: React.PropTypes.string.isRequired,
  repos: React.PropTypes.array.isRequired
}

export default Repos;
//
// var Repos = React.createClass({
//   propTypes: {
//     username: React.PropTypes.string.isRequired,
//     repos: React.PropTypes.array.isRequired
//   },
//   render: function(){
//     var repos  = this.props.repos.map(function(repo, index){
//       var url;
//       if (repo.html_url){
//         url = <h4><a href={repo.html_url}>{repo.name}</a></h4>;
//       }
//       return (
//         <li className="list-group-item" key={index}>
//           {url}
//           {repo.description && <p>{repo.description}</p>}
//         </li>
//      );
//    });
//     return (
//       <div>
//         <h3>User Repos</h3>
//         <ul className="list-group">
//           {repos}
//         </ul>
//       </div>
//     )
//   }
// });
//
// module.exports = Repos;
